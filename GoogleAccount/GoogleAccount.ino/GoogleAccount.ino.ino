/*  Google Calendar Integration ESP8266
 *  Created by Daniel Willi, 2016
 *
 *  Based on the WifiClientSecure example by
 *  Ivan Grokhotkov
 *
 */

#include <ESP8266WiFi.h>
#include "HTTPSRedirect.h"
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <sntp.h>
static const char ntpServerName[] = "1.asia.pool.ntp.org";
const int timeZone = 5;     // Central European Time

//Network credentials
const char* ssid = "Raj"; //replace with you ssid
const char* password = "Mater1al"; //replace with your password
//Google Script ID
const char *GScriptId = "1RKtH0pJQ57krPw3fGPusmHEYvEEu3KxZJA0a_NSsFwXwA7bhOS0PMMI7"; //replace with you gscript id
//const char *GScriptId = "1e4JAj9Im2l3_UJyXsRFHzeH1X6RCC6you3SuZTi1xyOGnFn4rqcHNYMe"; //

//Connection Settings
const char* host = "script.google.com";
const char* googleRedirHost = "script.googleusercontent.com";
const int httpsPort = 443;

//Fetch Google Calendar events
String url = String("/macros/s/") + GScriptId + "/exec";
//Delimiter for Events
const String delimeter = "|";

//Define port order: 0 is Port 16, 1 is Port 5
int portOrder[] = {16, 5, 4, 0, 2, 14, 12, 13, 15};
bool activePins[(int)( sizeof(portOrder) / sizeof(portOrder[0]))];

#define DEBUG   //If you comment this line, the DPRINT & DPRINTLN lines are defined as blank.
#ifdef DEBUG    //Macros are usually in all capital letters.
#define DPRINT(...)    Serial.print(__VA_ARGS__)     //DPRINT is a macro, debug print
#define DPRINTLN(...)  Serial.println(__VA_ARGS__)   //DPRINTLN is a macro, debug print with new line
#else
#define DPRINT(...)     //now defines a blank line
#define DPRINTLN(...)   //now defines a blank line
#endif
//uint32 current_stamp; 



WiFiUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets

time_t getNtpTime();
void digitalClockDisplay();
void printDigits(int digits);
void sendNTPpacket(IPAddress &address);



void setup() {
  Serial.begin(115200);
  Serial.flush();
  connectToWifi();

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());
  Serial.println("waiting for sync");
  setSyncProvider(getNtpTime);
  setSyncInterval(300);
  
  
  //setPortsAsOutput();
}

time_t prevDisplay = 0; // when the digital clock was displayed


void loop() {

  if (timeStatus() != timeNotSet) {
    if (now() != prevDisplay) { //update the display only if time has changed
      prevDisplay = now();
      digitalClockDisplay();
    }
  }
  
  
  fetchDataFromGoogle();
//  setActivePins();
  delay(10000);
}

void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(".");
  Serial.print(month());
  Serial.print(".");
  Serial.print(year());
  Serial.println();
}



//Turn active pins from array on
void setActivePins() {
  for (int i = 0; i < (int)( sizeof(activePins) / sizeof(activePins[0])); i++) {
    //Turn active pins on
    if (activePins[i]) {
      digitalWrite(portOrder[i], HIGH);

      DPRINT("ON : ");
      DPRINT(i);
      DPRINT(" = ");
      DPRINT(portOrder[i]);
      DPRINT("\n");
    }
    //Turn not active pins off
    else {
      digitalWrite(portOrder[i], LOW);

      DPRINT("OFF: ");
      DPRINT(i);
      DPRINT(" = ");
      DPRINT(portOrder[i]);
      DPRINT("\n");
    }

    //After turning the pin on or off set it to off
    activePins[i] = false;
  }
  DPRINTLN("============");
}
//Set all pins from array portOrder as output
void setPortsAsOutput() {
  for (int i = 0; i < (int)( sizeof(portOrder) / sizeof(portOrder[0])); i++) {
    pinMode(portOrder[i], OUTPUT);
  }
}
//Connect to wifi
void connectToWifi() {

  DPRINTLN();
  DPRINT("Connecting to wifi: ");
  DPRINTLN(ssid);
  // flush() is needed to print the above (connecting...) message reliably,
  // in case the wireless connection doesn't go through
  Serial.flush();


  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    DPRINT(".");
  }

  DPRINTLN("");
  DPRINTLN("WiFi connected");
  DPRINTLN("IP address: ");
  DPRINTLN(WiFi.localIP());
  //current_stamp = sntp_get_current_timestamp(); 
  // Use HTTPSRedirect class to create TLS connection
  HTTPSRedirect client(httpsPort);

  DPRINT("Connecting to ");
  DPRINTLN(host);

  bool flag = false;
  for (int i = 0; i < 5; i++) {
    int retval = client.connect(host, httpsPort);
    if (retval == 1) {
      flag = true;
      break;
    }
    else
      DPRINTLN("Connection failed. Retrying...");
  }

  Serial.flush();
  if (!flag) {
    DPRINT("Could not connect to server: ");
    DPRINTLN(host);
    DPRINTLN("Exiting...");
    return;
  }
}
//Get calendar entries from google
void fetchDataFromGoogle() {
  HTTPSRedirect client(httpsPort);
  if (!client.connected())
    client.connect(host, httpsPort);
  Serial.println("after connect");
  String data = client.getData(url, host, googleRedirHost);
  Serial.println ("Data: " + data);
  bool p = false;
  for (int i = 0; i < data.length(); i++) {
    if ((String)data[i] == delimeter)
      p = !p;
   
    if (p && (String)data[i] != delimeter) {
      if (isdigit(data[i])) {
        activePins[data[i] - '0'] = true;
      }
    }
    Serial.println(data);
  }
}

void printDigits(int digits)
{
  // utility for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

/*-------- NTP code ----------*/

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

time_t getNtpTime()
{
  IPAddress ntpServerIP; // NTP server's ip address

  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  // get a random server from the pool
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}


